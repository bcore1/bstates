class BStateMachine {
    _states = null

    constructor() {
        this._states = []
    }

    function change(state) {
        this.pop()
        this.push(state)
    }

    function push(state) {
        this._states.push(state)
        state._enter(this)
    }

    function pop() {
        if (this._states.len() > 0) {
            local state = this._states.pop()
            state._exit()
        }
    }

    function current() {
        local length = this._states.len()
        return length > 0 ? this._states[length - 1] : null
    }
}