# BStates - State machine module for Gothic 2 Online

```js
// SHARED-SIDE
class HelloMessage extends BPacketMessage {
    </ type = BPacketString />
    message = null
}

class InitialState extends BState {
    _settings = null

    constructor(settings) {
        this._settings = settings
    }

    function onEnter() {
        // Called when state machine enters to this state
        print("Entering")
    }

    function onExit() {
        // Called when state machine exiting from this state
        print("Exiting")
    }

    // Register events inside state.
    // Events lifecycle:
    // - onEnter: handlers are added
    // - onExit: handlers are removed
    </ event = "onInit", priority = 1 />
    function onServerReady() {
        print("SERVER IS READY!")
    }

    // NOTE! You need to import BPackets first to make it work.
    </ packet = HelloMessage />
    function onPacketReceived(pid, message) {
        print("RECEIVED FROM: " + getPlayerName(pid))
        print(message.message)
        this.machine.pop() // Exit this state
    }
}

local settings = {
    time = {hour = 8, minute = 0}
    camera = {
        position = Vec3(0, 0, 0),
        rotation = Vec3(0, 0, 0)
    }
}

global_state <- BStateMachine()
global_state.push(InitialState(settings))

if (CLIENT_SIDE) {
    addEventHandler("onInit", function () {
        local message = HelloMessage("Welcome in colony!")
        message.serialize().send(RELIABLE)
    })
}
```
