local BPACKET_LOADED = "BPacketMessage" in getroottable()

//--------------------------------------------

class BState {
    machine = null // Reference to machine that owns this state
    __events = null
    __packets = null

    ///////////////////////////////////////////
    /// Build-in events
    ///////////////////////////////////////////

    function onEnter() {
        // Called when we enter this state
    }

    function onExit() {
        // Callec before we exit from this state
    }

    ///////////////////////////////////////////
    /// Private
    ///////////////////////////////////////////

    function _enter(machine) {
        this._register()
        this.machine = machine
        this.onEnter()
    }

    function _exit() {
        this.onExit()
        this._unregister()
    }

    function _register() {
        local template = this.getclass()
        foreach (index, member in template) {
            local attrs = null
            if (type(member) == "function" && (attrs = template.getattributes(index))) {
                if ("event" in attrs) {
                    this._registerEvent(member, attrs)
                }

                if (BPACKET_LOADED && "packet" in attrs) {
                    this._registerPacket(member, attrs)
                }
            }
        }
    }

    function _unregister() {
        if (this.__events) {
            foreach (event in this.__events) {
                this._unregisterEvent(event)
            }
        }

        if (this.__packets) {
            foreach (packet in this.__packets) {
                this._unregisterPacket(packet)
            }
        }
    }

    function _registerEvent(member, attrs) {
        // Lazy loading
        if (this.__events == null) {
            this.__events = []
        }

        local priority = 9999
        if ("priority" in attrs) {
            priority = attrs.priority
        }

        local clousure = member.bindenv(this)
        this.__events.push({name = attrs.event, handler = clousure})

        addEventHandler(attrs.event, clousure, priority)
    }

    function _unregisterEvent(event) {
        removeEventHandler(event.name, event.handler)
    }

    function _registerPacket(member, attrs) {
        // Lazy loading
        if (this.__packets == null) {
            this.__packets = []
        }

        local clousure = member.bindenv(this)
        this.__packets.push({message = attrs.packet, handler = clousure})

        attrs.packet.bind(clousure)
    }

    function _unregisterPacket(packet) {
        packet.message.unbind(packet.handler)
    }
}
